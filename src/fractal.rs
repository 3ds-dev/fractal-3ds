use num_complex::{Complex, ComplexFloat};
use std::f32::consts::PI;

fn newton(z: Complex<f32>) -> Complex<f32> {
    let one: Complex<f32> = Complex::new(1.0, 0.0);
    let three: Complex<f32> = Complex::new(3.0, 0.0);
    let top = z.powu(3) - one;
    let bottom = three * z.powu(2);
    let fraction = top / bottom;
    return z - fraction;
}

fn float_range(start: f32, end: f32, step: f32) -> std::vec::IntoIter<f32> {
    let mut range: Vec<f32> = vec![];
    let mut current: f32 = start;
    while current < end {
        current += step;
        range.push(current);
    }
    range.into_iter()
}

const Z_1: Complex<f32> = Complex::new(1.0, 0.0);
const Z_2: Complex<f32> = Complex::new(0.5, 0.866); //Complex::from_polar(1.0, 2.0 * PI / 3.0);
const Z_3: Complex<f32> = Complex::new(0.5, -0.866); //Complex::from_polar(1.0, 4.0 * PI / 3.0);
const ROOTS: [Complex<f32>; 3] = [Z_1, Z_2, Z_3];

fn diff(n: Complex<f32>, o: Complex<f32>) -> f32 {
    ((n - o) / o).abs()
}

fn sort(input: [f32; 3]) -> [f32; 3] {
    let mut sort = input;
    if sort[1] > sort[2] {
        let bigger = sort[1];
        let smaller = sort[2];
        sort[1] = smaller;
        sort[2] = bigger;
    }
    if sort[0] > sort[1] {
        let bigger = sort[0];
        let smaller = sort[1];
        sort[0] = smaller;
        sort[1] = bigger;
    }
    sort
}

fn check_err(z: Complex<f32>) -> f32 {
    let mut diffs: [f32; 3] = [0.0; 3];
    for root in 0..3 {
        diffs[root] = diff(z, ROOTS[root]);
    }
    diffs = sort(diffs);
    diffs[0]
}
//800x240
pub fn pixel_to_complex(x: f32, y: f32, xoffset: f32, yoffset: f32, zoom: f32) -> Complex<f32> {
    //Should flip the image, hopefully
    Complex::new((y + xoffset) / zoom, ((x) + yoffset) / zoom)
}

pub fn iterations(z: Complex<f32>) -> u8 {
    let mut z = z;
    let mut iters: u8 = 0;
    while check_err(z) > 0.1 && iters < 254 {
        iters += 1;
        z = newton(z);
    }
    iters
}

pub fn u8_to_gbr(input: u8) -> [u8; 3] {
    let number = input as f32;
    let spread: f32 = number / 6.0;
    let g = -(spread - 15.0).powi(2) + 250.0;
    let b = -(spread - 40.0).powi(2) + 250.0;
    let r = -(spread - 65.0).powi(2) + 250.0;
    [g as u8, b as u8, r as u8]
}
