#![allow(unused)]
use ctru::gfx::Screen as _;
use ctru::prelude::*;

mod fractal;
fn main() {
    ctru::init();
    let gfx = Gfx::init().expect("Couldn't obtain GFX controller");
    let hid = Hid::init().expect("Couldn't obtain HID controller");
    let apt = Apt::init().expect("Couldn't obtain APT controller");
    let _console = Console::init(gfx.bottom_screen.borrow_mut());

    println!("\x1b[21;16HPress Start to exit.");

    let mut top_screen = gfx.top_screen.borrow_mut();

    // We don't need double buffering in this example.
    // In this way we can draw our image only once on screen.
    top_screen.set_double_buffering(false);

    // We assume the image is the correct size already, so we drop width + height.
    let frame_buffer = top_screen.get_raw_framebuffer();
    let drawing = draw_fractal();
    unsafe { frame_buffer.ptr.copy_from(drawing.as_ptr(), drawing.len()) }
    // Main loop
    while apt.main_loop() {
        //Scan all the inputs. This should be done once for each frame
        hid.scan_input();

        if hid.keys_down().contains(KeyPad::KEY_START) {
            break;
        }

        // Flush and swap framebuffers
        gfx.flush_buffers();
        gfx.swap_buffers();

        //Wait for VBlank
        gfx.wait_for_vblank();
    }
}

fn draw_fractal() -> [u8; 800 * 240 * 3] {
    let mut drawing: [u8; 800 * 240 * 3] = [0; 800 * 240 * 3];
    for y in 0..800 {
        for x in 0..240 {
            let percent_done = 1 + ((((y * 240) + x) * 100) / (800 * 240));
            println!("\x1b[20;16H {}%", percent_done);
            let num = fractal::pixel_to_complex(x as f32, y as f32, -200.0, -120.0, 1.0);
            let iters = fractal::iterations(num);
            let color = fractal::u8_to_gbr(iters);
            let order = (x * 3) + (y * 240 * 3);
            for i in 0..3 {
                drawing[order + i] = color[i];
            }
        }
    }
    drawing
    //800x240
}
