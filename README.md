# fractal-3DS

This is a 3ds app that renders newtonian fractals currently only $z^3-1=0$ is supported, but it is easy to add other equations.

## to compile:
 - install [DevKitPro](https://devkitpro.org/wiki/Getting_Started)
 - install 3ds-dev
 - add necessary directories to path and env variables
 - install cargo-3ds
 - cargo 3ds build


![image](./screenshots/_12.02.23_20.02.15.619.png)


### I think it's stuck...

It's not, just reaally slow


### Why, tho?

Because I can ?


### WTF is this ?

Fractals are really cool shapes calculated with complex numbers that are infinitely self-similar. 
This is the newtonian fractal from the equation $z^3-1=0$.
